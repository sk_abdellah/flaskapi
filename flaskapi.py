from flask import Flask, request, redirect, url_for, render_template, jsonify
from flask_pymongo import pymongo

app = Flask(__name__)

CONNECTION_STRING = "mongodb+srv://root:root@cluster2-zwqih.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('FlaskAPI')
veille_collection = pymongo.collection.Collection(db, 'veilles')


@app.route('/')
@app.route('/api/v1/')
def index():
    return render_template("doc.html")

@app.route('/api/v1/tools', methods = ['GET', 'POST'])
def tools(): 
    if(request.method == 'GET'): 
        veilles = veille_collection.find({})
        data = []
        for veille in veilles:
            data.append({
                'title': veille['title'],
                'description': veille['description'],
                'url' : veille['url'],
                'author' : veille['author'],
                'tag' : veille['tag']
        })
        return jsonify({'data': data}) 

    if(request.method == 'POST'):
        title = request.form['title']
        description = request.form['description']
        url = request.form['url']
        author = request.form['author']
        tag = request.form['tag']

        db.veilles.insert_one({"title": title,
                                         "description" : description,
                                         "url" : url,
                                         "author" : author,
                                         "tag" : tag})
        return render_template("doc.html")
